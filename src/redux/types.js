export const transactionsTypes = {
  addTransaction: 'addTransaction',
  removeTransaction: 'removeTransaction',
  getTransactions: 'getTransactions',
}

export const loginTypes = {
  login: 'login'
}

export const counterTypes = {
  increase: 'counterIncrease',
  decrease: 'counterDecrease',
  reset: 'counterReset'
}