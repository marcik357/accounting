import {counterReducer} from "../reducers/counter";
import {resetCounter, increaseCounter, decreaseCounter} from "../actions/counter";

test('should increase counter', () => {
    const initialState = {
        currentCounter: 0
    }
    const newState = {
        currentCounter: 1
    }
    expect(counterReducer(initialState, increaseCounter())).toEqual(newState)
})
test('should decrease counter', () => {
    const initialState = {
        currentCounter: 5
    }
    const newState = {
        currentCounter: 4
    }
    expect(counterReducer(initialState, decreaseCounter())).toEqual(newState)
})

test('should not decrease counter when 0', () => {
    const initialState = {
        currentCounter: 0
    }
    const newState = {
        currentCounter: 0
    }
    expect(counterReducer(initialState, decreaseCounter())).toEqual(newState)
})

// const initialState = {amount: 10}
// const action = {type: 'DELETE'}
// const newState = {amount: 10}
//
// const Item = styled.div`
//     font-size: 14px;
//     line-height: 28px;
// `