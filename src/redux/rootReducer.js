// Core
import { combineReducers } from "redux";

// Reducers

import { loginReducer as login } from "./reducers/login";
import { transactionsReducer as transactions } from "./reducers/transactions";
import { counterReducer as counter } from "./reducers/counter";

export const rootReducer = combineReducers({ counter, login, transactions });
