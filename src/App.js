import './App.css';
import { useState, useReducer } from "react";
import { Routes, Route} from 'react-router-dom';
import { Home, Login, User } from './pages';
import Header from './components/header';
import { SumContext, SumDispatchContext } from "./context/sumContext";

function App() {
    const [sum, setSum] = useState(123);
    /*const [theme, dispatch] = useReducer();
    function switchThemeToDark() {
        dispatch({
            type: "change_to_dark"
        })
    }
    function switchThemeToLight() {
        dispatch({
            type: "change_to_light"
        })
    }
    function themeReducer(theme, action) {
        switch(action.type) {

        }
    }*/
  return (
    <SumContext.Provider value={{sum, setSum}}>
        {/*<SumDispatchContext.Provider value={dispatch}>*/}
      <Header/>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/user' element={<User/>}/>
      </Routes>
        {/*</SumDispatchContext.Provider>*/}
    </SumContext.Provider>
  );
}

export default App;
