import {useState} from 'react';
import TransactionsList from '../transactions-list';

export default function TransactionsOverview() {
  const [isNewTransaction, setIsNewTransaction] = useState(false);
  return <>
    <div>
      <h2>Transactions Overview</h2>
      <button onClick={() => setIsNewTransaction(true)}>New transaction</button>
       {/*{isNewTransaction ? <NewTransactionForm /> : null}*/}
    </div>
    <TransactionsList />
  </>
}