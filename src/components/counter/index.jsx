import { useDispatch,useSelector } from "react-redux";
import {resetCounter, increaseCounter, decreaseCounter} from "../../redux/actions/counter";

export default function Counter() {
    const dispatch = useDispatch();
    const {currentCounter} = useSelector((state)=> state.counter);
    return <div>
        <span>{currentCounter}</span>
        <br/>
        <br/>
        <button onClick={()=> dispatch(decreaseCounter())}>-</button>
        <button onClick={()=> dispatch(increaseCounter())}>+</button>
        <br/>
        <button onClick={()=> dispatch(resetCounter())}>reset</button>
    </div>
}