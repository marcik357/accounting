import { useState } from "react";
import {useDispatch} from "react-redux";
import { loginAsync } from "../../redux/actions/login";

export default function Auth() {
  const [value, setValue] = useState('');
  const dispatch = useDispatch();
  return <div>
    <input type="text" placeholder="Your name" value={value} onChange={(evt)=> setValue(evt.currentTarget.value)}/>
    <button onClick={()=> dispatch(loginAsync(value))}>Log in</button>
  </div>
}