import { useEffect, useContext } from "react";
import { SumContext } from "../../context/sumContext";
import { getTransactionsAsync } from "../../redux/actions/transactions";
import TableRow from "../table-row"
import { useDispatch, useSelector } from "react-redux";

export default function TransactionsList() {
  const dispatch = useDispatch();
  const {setSum} = useContext(SumContext);
  const data = useSelector((state)=> state.transactions)
  useEffect(() => {
    dispatch(getTransactionsAsync());
  }, [dispatch]);
  useEffect(()=> {
    const currentSum = data.reduce((acc,item)=>{
      return acc + (item.type ==='debit' ? item.amount: - item.amount)
    }, 0);
    setSum(currentSum);
  },[data]);
  return <table>
    <tbody>
      <tr>
        <td>title</td>
        <td>amount</td>
        <td>type</td>
        <td>date</td>
        <td>delete</td>
      </tr>
      {data.map(row => <TableRow key={row.id} {...row} />)}
    </tbody>
  </table>
}