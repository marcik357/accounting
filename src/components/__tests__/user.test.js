import {cleanup, render, screen, fireEvent} from '@testing-library/react';
import {User} from '../../pages';

afterEach(cleanup);

test('should workd onClick', () => {
    const handleClick = jest.fn();
    const props = {
        onClick: handleClick
    }
    render(<User {...props}/>);
    const button = screen.getByText('click me!');
    fireEvent.click(button);
    expect(handleClick).toHaveBeenCalled();
    fireEvent.click(button);
    expect(handleClick).toHaveBeenCalledTimes(2);
})