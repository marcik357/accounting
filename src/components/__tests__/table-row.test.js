import TableRow from "../table-row";
import { render, screen, cleanup } from "@testing-library/react";
import renderer from 'react-test-renderer';

// describe('all tests', ()=> {
//     tests('should always pass', () => {
//         expect(true).toBe(true)
//     });
//     tests('should always pass 1', () => {
//         expect(true).toBe(true)
//     })
// })
afterEach(cleanup);

test('should always pass', () => {
    const value = 2
    expect(value).not.toBe(4)
})

it('should always pass 1', () => {
    expect(true).toBe(true)
})

test('should also pass', () => {
    const obj = {a:2, b:3}
    expect(obj).toEqual({a:2, b:3})
})

test('should component exist', () => {
    render(<TableRow id={1}/>);
    expect(screen.getByTestId('table-row-1')).toBeInTheDocument()
})

test('tests basic props', () => {
    const tableRowProps = {
        id: 1,
        title: 'tests title',
        amount: 100,
        type: 'credit',
        date: 1684327097885
    }
    render(<TableRow {...tableRowProps}/>);
    expect(screen.getByTestId('table-row-1')).toBeInTheDocument();
    expect(screen.getByTestId('table-row-1')).toHaveTextContent('tests title');
    expect(screen.getByText('tests title')).toBeInTheDocument();
    expect(screen.getByTestId('table-row-1')).toHaveTextContent('$100');
    expect(screen.getByTestId('table-row-1')).toHaveTextContent('17.05');
    // fireEvent.click();
    // expect(click).toHaveBeenCalled()
})

test('tests debit props', () => {
    const tableRowProps = {
        id: 1,
        title: 'tests debit title',
        amount: 500,
        type: 'debit',
        date: 1684327097885
    }
    render(<TableRow {...tableRowProps}/>);
    expect(screen.getByText('debit')).toBeInTheDocument();
    expect(screen.getByText('debit')).toHaveStyle("color: #007700");
    // expect(screen.getByTestId('table-row-1')).toHaveTextContent('17.05');
})

test('tests credit props', () => {
    const tableRowProps = {
        id: 1,
        title: 'tests debit title',
        amount: 500,
        type: 'credit',
        date: 1684327097885
    }
    render(<TableRow {...tableRowProps}/>);
    expect(screen.getByText('credit')).toBeInTheDocument();
    expect(screen.getByText('credit')).toHaveStyle('color: #770000; font-style: italic');
    // expect(screen.getByTestId('table-row-1')).toHaveTextContent('17.05');
})

test('snapshot row', () => {
    const tableRowProps = {
        id: 1,
        title: 'tests debit title',
        amount: 500,
        type: 'credit',
        date: 1684327097885
    }
    const tree = renderer.create(<TableRow {...tableRowProps}/>).toJSON();
    expect(tree).toMatchSnapshot();
})
