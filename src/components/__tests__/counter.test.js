import {render, fireEvent, screen} from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import Counter from "../counter";
let mockStore = configureStore();
let store;

beforeEach(()=> {
    store = mockStore({counter : {currentCounter: 0}})
})

test('should render correctly', () => {
    render(
        <Provider store={store}>
            <Counter/>
        </Provider>
    )
    expect(screen.getByText('0')).toBeInTheDocument();
})

test('should increase on plus click', () => {
    render(
        <Provider store={store}>
            <Counter/>
        </Provider>
    );
    const plusButton = screen.getByText('+');
    fireEvent.click(plusButton);
    const actions = store.getActions();
    expect(actions).toEqual([{type: 'counterIncrease'}]);
})

test('should decrease on minus click', () => {
    render(
        <Provider store={store}>
            <Counter/>
        </Provider>
    );
    const minusButton = screen.getByText('-');
    fireEvent.click(minusButton);
    const actions = store.getActions();
    expect(actions).toEqual([{type: 'counterDecrease'}]);
})

test('should reset', () => {
    render(
        <Provider store={store}>
            <Counter/>
        </Provider>
    );
    const resetButton = screen.getByText('reset');
    fireEvent.click(resetButton);
    const actions = store.getActions();
    expect(actions).toEqual([{type: 'counterReset'}]);
})

// react -> action -> reducer -> store -> react