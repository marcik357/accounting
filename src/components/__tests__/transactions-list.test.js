import {render} from "@testing-library/react";
import configureStore from 'redux-mock-store';
import TransactionsList from "../transactions-list";

const mockStore = configureStore([]);

let store;

beforeEach(()=> {
    store = mockStore([])
});

