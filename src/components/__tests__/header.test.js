import {cleanup, render, screen, fireEvent} from '@testing-library/react';
import Header from '../header';
import {SumContext} from "../../context/sumContext";

afterEach(cleanup);

test('should show context', () => {
    const mockContext = 150;
    render(
        <SumContext.Provider value={{sum: mockContext}}>
            <Header/>
        </SumContext.Provider>
    )
    expect(screen.getByTestId('header-context')).toHaveTextContent('150');
})

test('should toggle theme from light to dark and vice versa', () => {
    render(<SumContext.Provider value={{sum: 100}}>
        <Header/>
    </SumContext.Provider>);
    const button = screen.getByText('toggle theme');
    const themeContainer = screen.getByTestId('theme');
    fireEvent.click(button);
    expect(themeContainer).toHaveTextContent('current theme: dark');
    fireEvent.click(button);
    expect(themeContainer).toHaveTextContent('current theme: light');
})